Commerce Discount Notification
---------------------

CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * UnInstallation
  * Maintainers

INTRODUCTION:
------------
This module sends out a notification when:-

- Particular discount code usage count reaches to the maximum limit
- This will help site administrator to monitor the discounts

REQUIREMENTS:
-------------
This module requires the following module:-

1. https://www.drupal.org/project/commerce_coupon

INSTALLATION:
-------------
1. Copy 'commerce_discount_notification' folder to modules directory
2. At admin/modules enable the 'commerce_discount_notification' module

CONFIGURATION:
-------------
1. Make sure that you have installed drupal commerce and commerce discounts
2. Activate module
3. Go to admin/commerce/discounts/commerce-discount-notification
4. Select the checkbox - Send Commerce Discount Notification
5. Enter email address in 'Email(s) to send notification to' field
6. Enter email subject and body
7. Click on 'Save Configuration' button
8. Add some discount and coupon codes
9. Set max usage limit to each discount
10. Apply the coupon code on checkout page

If that particular discount code usage limit reaches to the maximum limit set
This module will send an email to the email ids set in configuration form
(Step 5 in above configuration points)

UN-INSTALLATION:
----------------
To un-install the module:-
1. Go to admin/modules
2. Find 'commerce_discount_notification' in OTHER package
3. Un-check the checkbox and save
4. Then go to admin/modules/uninstall
5. Un-check the checkbox and click 'uninstall'

MAINTAINERS:
------------
Hiraman Patil - https://www.drupal.org/u/hiramanpatil
