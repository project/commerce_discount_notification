<?php

/**
 * @file
 * Commerce Commerce Discount Notification admin file.
 */

/**
 * Implements hook_form().
 */
function _commerce_discount_notification_admin_form($form, &$form_state) {

  $form['commerce_discount_notification_send_mail'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send Commerce Discount Notification'),
    '#default_value' => variable_get('commerce_discount_notification_send_mail', ''),
    '#required' => TRUE,
  );

  $form['commerce_discount_notification_users_list'] = array(
    '#type' => 'textfield',
    '#title' => t('Email(s) to send notification to'),
    '#description' => t('For multiple emails enter comma separated values.'),
    '#default_value' => variable_get('commerce_discount_notification_users_list', ''),
    '#states' => array(
      'visible' => array(':input[name=commerce_discount_notification_send_mail]' => array('checked' => TRUE)),
    ),
  );

  $form['notification_email_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Content'),
    '#states' => array(
      'visible' => array(':input[name=commerce_discount_notification_send_mail]' => array('checked' => TRUE)),
    ),
  );
  $form['notification_email_content']['commerce_discount_notification_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Mail subject'),
    '#default_value' => variable_get('commerce_discount_notification_subject', 'Discount code has been reached to the maximum limit - [#CODE]'),
    '#states' => array(
      'visible' => array(':input[name=commerce_discount_notification_send_mail]' => array('checked' => TRUE)),
    ),
  );
  $form['notification_email_content']['commerce_discount_notification_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Mail body'),
    '#default_value' => variable_get('commerce_discount_notification_body', 'Hello,
    The discount code [#CODE] has been reached to the maximum limit. Login as a administrator and review the discount and its usage.
    Thanks'),
    '#states' => array(
      'visible' => array(':input[name=commerce_discount_notification_send_mail]' => array('checked' => TRUE)),
    ),
  );

  $form['#validate'][] = '_commerce_discount_notification_admin_form_validate';
  return system_settings_form($form);
}

/**
 * Validation function for _commerce_discount_notification_admin_form.
 */
function _commerce_discount_notification_admin_form_validate($form, &$form_state) {

  // Get variable values.
  $send_mail = $form_state['values']['commerce_discount_notification_send_mail'];
  $users_list = $form_state['values']['commerce_discount_notification_users_list'];
  $subject = $form_state['values']['commerce_discount_notification_subject'];
  $body = $form_state['values']['commerce_discount_notification_body'];

  // Validate fields.
  if ($send_mail) {

    if (trim($users_list) == '') {
      form_set_error('commerce_discount_notification_users_list', t('Enter Emailes) to send notificatio.'));
    }

    if (trim($subject) == '') {
      form_set_error('commerce_discount_notification_subject', t('Enter email subject.'));
    }

    if (trim($body) == '') {
      form_set_error('commerce_discount_notification_body', t('Enter email body.'));
    }

    if ($users_list) {
      $users = explode(',', $users_list);
      foreach ($users as $email) {
        if (!valid_email_address($email)) {
          form_set_error('commerce_discount_notification_users_list', t('Invalid e-mail has been detected.'));
        }
      }
    }

  }

}
